# builder_pattern4j

## 1 项目介绍

本项目使用Java语言对建造者模式进行实现。实现上使用一些语法糖简化代码。

## 2 架构介绍

```
.
├── pom.xml
├── README.md
├── Build pattern.md   # 原始markdown文档
├── Build pattern.html # 转换出的html文档
└── src
    ├── builder # 建造者
    |   ├── Builder.java       # 建造者接口
    |   └── HTMLBuilder.java   # HTML建造者
    |
    ├── Storer.java # 数据存储类
    └── SetUp.java # 程序入口
```

## 3 内容说明

### 3.1 项目构建

本项目JDK版本为JDK1.8，使用maven作为包管理工具.

### 3.2 语法糖

1. try-with-resources: 将需要关闭的流放到try中，try块结束后，自动关闭资源。
* 使用条件：需要自动关闭的流应是实现了AutoCloseable接口的类。
```
try(BufferedWriter bw = new BufferedWriter(new FileWriter("Build pattern.html"))){
    bw.write(content);
}catch (IOException e){
    System.out.println(e.getMessage());
}
```

## 4 项目贡献

本项目由Chemxy提供，仅供参考使用，不提供任何保证，禁止用于其他用途。