# Build pattern

## Homework purpose

Design and implement a document generator that can simultaneously expand the types of markup languages and generate document types using the Builder pattern.

## Homework content

Design and implement a document generator using the Builder pattern, which supports multiple markup languages and can generate multiple documents.
