import builder.Builder;
import builder.HTMLBuilder;

import java.io.IOException;
import java.util.List;

public class SetUp {

    public static void main(String[] args) {
        List<String> contents = Storer.read();

        String htmlString = new HTMLBuilder()
                .header("Build pattern")
                .body(contents)
                .footer()
                .build();

        Storer.write(htmlString);
    }
}
