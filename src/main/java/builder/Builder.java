package builder;

import java.util.List;

public interface Builder {
    Builder header(String content);
    Builder body(List<String> contents);
    Builder footer();
    String build();
}
