package builder;

import java.util.List;

public class HTMLBuilder implements Builder{
    private final StringBuilder stringBuilder = new StringBuilder();
    @Override
    public Builder header(String content) {
        stringBuilder.append("<html>\n")
                .append("<head>\n").
                append("<title>").append(content).append("</title>\n")
                .append("</head>\n");
        return this;
    }

    @Override
    public Builder body(List<String> contents) {
        stringBuilder.append("<body>\n");
        for(String line : contents){
            if(line.startsWith("# ")){
                addFLTitle(line.substring(2));
            } else if (line.startsWith("## ")) {
                addSLTitle(line.substring(3));
            } else if (!line.isEmpty()) {
                addParagraph(line);
            }
        }
        stringBuilder.append("</body>\n");
        return this;
    }
    private void addFLTitle(String content){
        stringBuilder.append("<h1>").append(content).append("</h1>\n");
    }
    private void addSLTitle(String content){
        stringBuilder.append("<h2>").append(content).append("</h2>\n");
    }
    private void addParagraph(String content){
        stringBuilder.append("<p>\n").append(content).append("\n").append("</p>\n");
    }

    @Override
    public Builder footer() {
        stringBuilder.append("</html>");
        return this;
    }

    @Override
    public String build() {
        return stringBuilder.toString();
    }
}
