import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Storer {
    public static List<String> read()  {
        try(BufferedReader br = new BufferedReader(new FileReader("Build pattern.md"))){
            List<String> contents = new ArrayList<>();
            String line;
            while((line = br.readLine())!=null){
                contents.add(line);
            }

            return contents;
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        return null;
    }

    public static void write(String content){
        try(BufferedWriter bw = new BufferedWriter(new FileWriter("Build pattern.html"))){
            bw.write(content);
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
